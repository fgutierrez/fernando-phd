import numpy as np

# Function to sort the matrix
def sortMat(data, row, col):
    # Number of elements in matrix
    size = row * col
    # Loop to sort the matrix
    # using Bubble Sort
    for i in range(0, size):
        for j in range(0, size - 1):

            # Condition to check
            # if the Adjacent elements
            if (data[j // col][j % col] > \
                    data[(j + 1) // col][(j + 1) % col]):
                # Swap if previous value is greater
                temp = data[j // col][j % col]
                data[j // col][j % col] = \
                    data[(j + 1) // col][(j + 1) % col]
                data[(j + 1) // col][(j + 1) % col] = \
                    temp
    return 0



def printMat(mat, row, col):
    # Loop to print the matrix
    n = []
    #n = np.ndarray(shape=(row, col), dtype=float)
    for i in range(row):
        for j in range(col):
            n.append(mat[i][j])
            #print(mat[i][j], end=" ")
        #print()
    #print(n)
    return n
    # np.savetxt("sorted_matrix.csv", mat, delimiter=" ")


# Driver Code to test the algorithm
if __name__ == "__main__":
#    mat = [[4.23174704e-03, 6.93280127e-03, 6.96444057e-03, 7.83797792e-03,
#  1.91510524e-02, 1.99217787e-02],
# [3.66144336e-02, 4.22280686e-02, 1.18723910e-01, 1.26870515e-01,
#  1.55778944e-01, 1.93539443e-01],
# [2.06268384e-01, 2.67926574e-01, 2.94416540e-01, 5.61417696e-01,
#  5.64538630e-01, 5.99514848e-01],
# [6.88082895e-01, 8.75505325e-01, 1.93868529e+00, 2.61831038e+00,
#  2.72696755e+00, 3.29258765e+00],
# [4.46267398e+00, 4.76064871e+00, 5.31642052e+00, 7.37258061e+00,
#  7.50853362e+00, 8.76764159e+00],
# [8.94828413e+00, 9.53025759e+00, 1.20190512e+01, 1.47958722e+01,
#  2.06109909e+01, 2.51410219e+01]]

#    mat = [[7.03457901e-01, 9.23325718e-01, 2.71577077e+00, 4.37231794e+00,
#        1.08373748e+01, 2.44076370e+01],
#       [7.28910269e-03, 1.20112480e-01, 1.77438290e-01, 5.19696418e-01,
#        7.02216962e+00, 1.55767965e+01],
#       [3.99800801e-03, 1.25725706e-02, 1.29305871e-02, 6.68636519e-02,
#        2.56527614e+00, 1.02107757e+01],
#       [7.79967608e-03, 2.90484070e-02, 8.17818390e-02, 1.99574561e-01,
#        4.58401289e+00, 1.13149107e+01],
#       [6.00909455e-02, 1.36472072e-01, 6.86916852e-01, 8.59293792e-01,
#        1.00261062e+01, 2.47481004e+01],
#       [1.52908459e+00, 1.57357502e+00, 4.02176133e+00, 5.23582762e+00,
#        2.38829280e+01, 5.83775556e+01]]

    mat = [[1.46483493e+02, 7.33532638e+01, 6.48301694e+01, 8.42442189e+01,
        1.22954501e+02, 2.26153873e+02],
       [4.65185452e+01, 1.48377018e+01, 2.41141547e+01, 2.38242199e+01,
        4.88933536e+01, 1.05718662e+02],
       [1.49558289e+01, 5.23279677e+00, 2.25779948e+00, 1.33394102e+00,
        2.44364734e+01, 6.16356206e+01],
       [9.98561899e-01, 2.03278876e-01, 3.57415983e-02, 1.11950685e-02,
        2.83134284e+00, 3.81092755e+01],
       [1.35853782e+00, 1.98855077e-01, 2.15732531e-02, 2.34435966e-02,
        7.15551778e-01, 2.32342475e+01],
       [4.46812996e+00, 6.23773645e-01, 7.44775013e-02, 7.62986696e-02,
        1.87168910e+01, 5.46308410e+01]]
    row = len(mat)
    col = len(mat[0])

    # Function call to sort
    sortMat(mat, row, col)

    # Function call to print the matrix
    N = printMat(mat, row, col)
    print(mat)
    #np.ndarray(shape=(col, row), dtype=float)
