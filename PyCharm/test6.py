import numpy as np
import matplotlib as plt
from pylab import *
from astropy.io import fits as pyfits
import struct
import spline2dbase

# PSF characteristics
sizex = 8  # physical size of the PSF x-axis
sizey = 8  # physical size of the PSF y-axis
#ffits = 'psf128.fits' # name of the FITS file containing the PSF
ffits = '6000-14047-280196.fits' # Name of the FITS file containing the PSF
#ffits = 'tour_eiffel.fits'
subres = 128 # resolution of the PSF (fits file)
bsres = 20 # resolution of the b-spline decomposition of the PSF


# Imagette characteristics
sizex2 = 6  # physical size of the imagette x-axis
sizey2 = 6  # physical size of the imagette y-axis
xc,yc  = 5.,4. # star centroid within the imagette domain



# Defining the barycenter
def barycenter(array,mask=None,x=None,y=None,subres=1):
    if(isinstance(x, (np.ndarray, np.generic) )== False):
        x=(np.arange(0,array.shape[1])+0.5)/float(subres)
    if(isinstance(y, (np.ndarray, np.generic) )== False):
        y=(np.arange(0,array.shape[0])+0.5)/float(subres)

    if(x.ndim ==1 & y.ndim ==1):
        x,y = np.meshgrid(x,y)

    if(mask is not None):
        weight=np.sum(array*mask)
        bx= np.sum(array*x*mask)/weight
        by= np.sum(array*y*mask)/weight
    else:
        tmp=np.sum(array)
        bx= np.sum(array*x)/tmp
        by= np.sum(array*y)/tmp
    return bx,by

psf = np.array(pyfits.open(ffits)[0].data,dtype=np.float)
psf /= psf.sum()
pxc,pyc = barycenter(psf,subres=subres)

poffy = pyc - psf.shape[0]/2./subres
poffx = pxc - psf.shape[1]/2./subres

print('C.O.B. PSF domain:', pxc,pyc)
print('Shift in C.O.B. PSF', poffx,poffy)


lx = bsres*sizex
ly = bsres*sizey
psfbs = spline2dbase.Pixel2Spline(psf, lx ,ly)

offx = xc-pxc
offy = yc-pyc

# imagette obtained by integrating the b-spline decomposition of the PSF
imgagette = spline2dbase.Spline2Imagette(psfbs,bsres,sizex2,sizey2,offx=offx,offy=offy)
# writing a csv file with the values of the imagette
np.savetxt("contaminant.csv", imgagette, delimiter=" ")

#Now the function
# img10 = spline2dbase.Spline2ImagetteMulti(psfbs,bsres,sizex,sizey,np.array([0.,1.5]),np.array([0.,2.]))

# Defining the barycenter of the imagette
COBx,COBy= barycenter(imgagette,subres=1)

print ('Imagette C.O.B. coordinates: %f, %f' % (COBx,COBy))
#print('Imagette:', imgagette)
#print('La variable imgagette es del tipo:',type(imgagette))
#print('First element',imgagette[0])


close('all')

#Showing the images
figure(1)
clf()
imshow(psf,origin='lower',interpolation=None)


figure(2)
clf()
imshow(imgagette,origin='lower',interpolation=None)

#Showing the grid in the imagette
plt.hlines(y = np.arange(-0.5,5.5,1), xmin = -0.5, xmax = 5.5, color = 'white', linestyles='dashed')
plt.vlines(x = np.arange(-0.5,5.5,1), ymin = -0.5, ymax = 5.5, color = 'white', linestyles='dashed')

#Showing the C.O.B. of the target/contaminant
plt.plot(4.5,3.5,'o', linewidth=1, color = 'black')

draw()
show(block=False)
plt.show()


# Continuing here 24/08/2022

