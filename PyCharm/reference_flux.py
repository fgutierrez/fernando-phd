from typing import Union, Any

import numpy as np

# Let's create a function for obtaining the reference flux for a target star in the PLATO band for a normal camera
def reference_flux_target(p, zp):
    """
    :param p: Magnitude of the star in the PLATO band
    :param zp: Zero point of the PLATO band
    """
    fp = (10 ** (-0.4 * (p - zp))) * 21.
    #print('The reference flux for a 6000 K G0V star of magnitude P =', p ,'after the integration time, but without '
                                                                          #'brightness attenuation is:', "%.2f" % fp,
          #'e-')
    return fp


# Let's create a function for obtaining the reference flux for a contaminant star in the PLATO band for a normal camera
def reference_flux_contaminant(ft, mc, mt):
    """
    :param mt: Magnitude of the target star in the PLATO band
    :param mc: Magnitude of the contaminant star in the PLATO band
    """
    fc = (ft * 10 ** (-0.4 * (mc - mt))) * 21.
    return fc

def flux_pixel_target(a, f):
    return a * f

def flux_pixel_contaminant(b, f):
    return b * f

# Driver code
if __name__ == '__main__':
    p = 11.16
    zp = 20.62
    f = np.array([[5.12425154e-05, 1.68465960e-04, 2.37235956e-04, 1.02844778e-04,
                   4.81168627e-05, 2.22387211e-05],
                  [1.75474645e-04, 7.73693179e-04, 4.10680200e-03, 3.08525392e-03,
                   9.64642417e-05, 3.81556984e-05],
                  [2.45916174e-03, 1.52861045e-02, 1.66805526e-01, 4.06344573e-01,
                   8.33414767e-04, 6.17109071e-05],
                  [1.75771644e-03, 1.29182241e-02, 1.65846455e-01, 1.37426760e-01,
                   3.32029364e-03, 1.01196192e-04],
                  [5.27729806e-04, 3.82951937e-03, 3.49801844e-02, 3.39604057e-02,
                   1.25631057e-04, 4.30230457e-05],
                  [1.39459357e-04, 6.73149200e-04, 1.59838436e-03, 8.37353005e-04,
                   6.91635494e-05, 2.02126833e-05]])

    # Let's call the function that calculates the reference flux for the target
    reference_flux_target(p, zp)

    # Let's call the function that calculates the reference flux for the contaminant
    #reference_flux_contaminant(reference_flux(p, zp), mc, mt)