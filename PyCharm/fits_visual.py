# Let's import the main libraries
import matplotlib.pyplot as plt
from astropy.io import fits
from astropy.utils.data import get_pkg_data_filename
import astropy.visualization as apyvi
plt.style.use(apyvi.astropy_mpl_style)

# Let's define the function that will create the image of the Fits file
def fits_visualiser(path):
    image_file = get_pkg_data_filename(path)
    image_data = fits.getdata(image_file)
    plt.imshow(image_data, cmap="viridis", origin='lower')
    plt.grid(False)
    plt.show()

# Driver code. Here you can test it
if __name__ == '__main__':
    fits_visualiser('tour_eiffel.fits')
