import numpy as np
import matplotlib.pyplot as plt
from matrix_sorter import sortMat, printMat
from solar_system_planets import planets, print_etas


# Let's read the CSV file created by the test6.py file
csv_data = open('etas.csv')

# Let's assign the csv data to a variable
imagette = np.loadtxt(csv_data, delimiter=' ')

# Let's multiply the 2-D array called 'imagette' by the reference flux of one camera for a star of magnitude P = 11
# (7046.930689671467 e-/s) then for the 24 cameras (24) and then for the integration time (21s):
def flux_pixel_target(a):
    return (a*3551653.0675944197)/24.

# Let's call the function
f_pix_t = flux_pixel_target(imagette)

# Let's plot that function
plt.imshow(f_pix_t, origin = 'lower')
plt.show()

# Let's compute now the NSR of the target
def nsr(sb, sq, sd, f):

    N = np.sqrt(sb**2 + sq**2 + sd**2 + f)/f

    return N

NSR_imagette = nsr(45, 7.2, 50.2, f_pix_t)

plt.imshow(NSR_imagette, origin = 'lower')
plt.show()

# Let's sort the matrix in increasing order. By this, I mean that every element of every row has to be sorted in
# increasing order and that the first element of each row has to be higher than the first element of the previous row.
sortMat(NSR_imagette, len(NSR_imagette), len(NSR_imagette[0]))
nsr_imagette_sorted = printMat(NSR_imagette, len(NSR_imagette), len(NSR_imagette[0]))

# Let's make it a 2-D array
nsr_imagette_sorted = np.reshape(np.array(nsr_imagette_sorted), (-1,6))

# Let's print it
#print(nsr_imagette_sorted)

plt.imshow(nsr_imagette_sorted, origin = 'lower')
plt.show()


# Let's reshape now f_pix_t into a 1-D array
f_pix_1d = f_pix_t.ravel()

# Let's now sort it in increasing order
f_pix_t_1d_sorted = np.sort(f_pix_1d)[::-1]

# Let's calculate now the numerator of NSR_agg
def numerator_agg(sb,sq,sd,f):
    c = []
    d = 0
    for i in range(0,len(f)):
        d = d + sb**2 + sq**2 + sd**2 + f[i]
        c.append(d)
    return c

# Let's save the result in a numpy array
numerator_nsragg = np.array(np.sqrt(numerator_agg(45,7.2,50.2,f_pix_t_1d_sorted)))

# Let's calculate now the denominator of NSR_agg
def denominator_agg(F):
    a = [F[0]]
    b = F[0]
    for i in range(1,len(F)):
        b = b + F[i]
        a.append(b)
    return a

# Let's save the result in a numpy array
denominator_nsragg = np.array(denominator_agg(f_pix_t_1d_sorted))

# Let's calculate now NSR_agg by making the division of its numerator over its denominator
def nsr_agg(numerator,denominator):
    return numerator/denominator

# Let's make the division
NSR_agg = nsr_agg(numerator_nsragg,denominator_nsragg)

# Let's now calculate NSR_1h
def nsr1h(ncameras, nsragg):
    return 10**6/(12*np.sqrt(ncameras))*nsragg

# Let's calculate NSR_1h with 24 cameras
NSR1h_24 = nsr1h(24,NSR_agg)

# Let's calculate NSR_1h with 6 cameras as well
NSR1h_6 = nsr1h(6,NSR_agg)

# Let's plot it
plt.plot(NSR1h_24, 'o-')
plt.xlabel('Number of pixels $m$  composing the binary mask', fontsize=14)
plt.ylabel('$ NSR_{1h}[ppm \, \sqrt{hr}] $', fontsize = 14)
#plt.savefig('binary_mask.pdf', format='pdf', bbox_inches='tight', dpi=300)
plt.show()


# Let's write some values for the solar system planets. First the name, then the transit depth in ppm, the transit duration in hours and the number of observed transits
mercury = planets("Mercury", 0.0012 * 10000, 8.1, 3)
venus = planets("Venus", 0.0080 * 10000, 11.0, 3)
earth = planets("Earth", 0.0084 * 10000, 13.0, 3)
mars = planets("Mars", 0.0023 * 10000, 16.0, 3)
jupiter = planets("Jupiter", 1.01 * 10000, 30.0, 3)
saturn = planets("Saturn", 0.68 * 10000, 40.0, 3)
uranus = planets("Uranus", 0.116 * 10000, 57.0, 3)
neptune = planets("Neptune", 0.096 * 10000, 71.0, 3)

# Let's calculate the statistical significance creating the following function
def eta(depth, td, ntr, spr, nsr):
    term1 = depth * np.sqrt(td * ntr)
    term2 = (1 - spr) / min(nsr)
    return term1 * term2


# Let's calculate the statistical significance function
mercury_eta = eta(mercury.depth, mercury.duration, mercury.number, 0, NSR1h_24)
venus_eta = eta(venus.depth, venus.duration, venus.number, 0, NSR1h_24)
earth_eta = eta(earth.depth, earth.duration, earth.number, 0, NSR1h_24)
mars_eta = eta(mars.depth, mars.duration, mars.number, 0, NSR1h_24)
jupiter_eta = eta(jupiter.depth, jupiter.duration, jupiter.number, 0, NSR1h_6)
saturn_eta = eta(saturn.depth, saturn.duration, saturn.number, 0, NSR1h_6)
uranus_eta = eta(uranus.depth, uranus.duration, uranus.number, 0, NSR1h_6)
neptune_eta = eta(neptune.depth, neptune.duration, neptune.number, 0, NSR1h_24)


# Let's print the statistical significances of the planets of our solar system
print_etas(mercury.name, mercury_eta)
print_etas(venus.name, venus_eta)
print_etas(earth.name, earth_eta)
print_etas(mars.name, mars_eta)
print_etas(jupiter.name, jupiter_eta)
print_etas(saturn.name, saturn_eta)
print_etas(uranus.name, uranus_eta)
print_etas(neptune.name, neptune_eta)