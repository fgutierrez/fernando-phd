import numpy as np


def etat(SPRT, td, ntr_t, dbeb, nsr):
    term1 = (1 - SPRT) * np.sqrt(td * ntr_t)
    term2 = dbeb / min(nsr)
    return term1 * term2


def etac(SPRC, td, ntr_c, dbeb, nsr):
    term1 = (SPRC) * np.sqrt(td * ntr_c)
    term2 = dbeb / min(nsr)
    return term1 * term2
