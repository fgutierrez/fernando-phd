import numpy as np
from numpy import unravel_index

#class():
#    def __init__(self, id, original, sorted):
#        self.id = id                # id of the pixel
#        self.original = original    # coordinate of the pixel in the original imagette
#        self.sorted = sorted        # coordinate of the pixel in the sorted imagette

def sorted_2_nsr(f):
    for i in range(len(f)):
        for j in range(len(f[i])):
            print(f[i][j], end=" ")
        print()

def indexes(mat):
    x = mat.flatten().tolist()

    for i in range(len(mat)):
        for j in range(len(mat[i])):
            index = x.index(mat[i][j])
            print(np.unravel_index(index, mat.shape))
            print(mat[i][j])

if __name__ == "__main__":
     f = [[4.23174704e-03, 6.93280127e-03, 6.96444057e-03, 7.83797792e-03,
  1.91510524e-02, 1.99217787e-02],
 [3.66144336e-02, 4.22280686e-02, 1.18723910e-01, 1.26870515e-01,
  1.55778944e-01, 1.93539443e-01],
 [2.06268384e-01, 2.67926574e-01, 2.94416540e-01, 5.61417696e-01,
  5.64538630e-01, 5.99514848e-01],
 [6.88082895e-01, 8.75505325e-01, 1.93868529e+00, 2.61831038e+00,
  2.72696755e+00, 3.29258765e+00],
 [4.46267398e+00, 4.76064871e+00, 5.31642052e+00, 7.37258061e+00,
  7.50853362e+00, 8.76764159e+00],
 [8.94828413e+00, 9.53025759e+00, 1.20190512e+01, 1.47958722e+01,
  2.06109909e+01, 2.51410219e+01]]

     sorted_2_nsr(f)

     indexes(f)