# This is the main python file for testing Marchiori's method for detecting false positives
import numpy as np
from numpy import unravel_index
from tabulate import tabulate
import matplotlib.pyplot as plt
from matrix_sorter import sortMat, printMat
from reference_flux import reference_flux_target, flux_pixel_target, reference_flux_contaminant, flux_pixel_contaminant
from plots_visual import ploting_imagettes, ploting_nsr, ploting_nsr_s
from NSR import NSRn, denominator_agg, numerator_agg, nsr_agg, nsr1h
from flux_from_NSR import indexes
from spr_marchiori import numerator_SPR_t, denominator_SPR_t, sprt, numerator_SPR_c, denominator_SPR_c, sprc
from etas_marchiori import etat, etac

# Let's use the latex stuff
#plt.rcParams.update({
#    "text.usetex": True
#})

# Let's obtain the value of the reference flux after the integration time for the target star
f_ref_t = reference_flux_target(11, 20.62)

# Let's obtain the value of the reference flux after the integration time for the contaminant star
f_ref_c = reference_flux_contaminant(f_ref_t, 16, 11)

# Let's call the imagette for the target from test6.py
target_data = open('target.csv')
# noinspection PyTypeChecker
target_imagette = np.loadtxt(target_data, delimiter=' ')

# Let's call the imagette for the contaminant from test6.py
contaminant_data = open('contaminant.csv')
# noinspection PyTypeChecker
contaminant_imagette = np.loadtxt(contaminant_data, delimiter=' ')

# Let's calculate the flux per pixel of the imagette of the target star
f_pix_t = flux_pixel_target(f_ref_t, target_imagette)

# Let's calculate the flux per pixel of the imagette of the contaminant star
f_pix_c = flux_pixel_contaminant(f_ref_c, contaminant_imagette)
f_pix_c[f_pix_c == 0] = 0.08
# Let's plot both Imagettes
ploting_imagettes(2, 1, f_pix_t, f_pix_c)

# Let's calculate the NSR of the imagette of the system. This is, the imagette with the target and the contaminant
nsr = NSRn(45., 50.2, 7.2, f_pix_t, f_pix_c)

# Let's plot now the NSR of the imagette of the system
ploting_nsr(nsr)
#print("This is nsr = \n{} \n".format(nsr))
#max_nsr_index = nsr.argmax()
#print(max_nsr_index)
#print(unravel_index(nsr.argmax(), nsr.shape))
#indexes(nsr)
table = nsr
print("This is nsr\n", tabulate(table, showindex = True))
#plt.plot(nsr)
#plt.show()

# Let's sort the NSRn in increasing order. This is, rearrange the pixels in increasing order of NSRn.
# This algorithm rearranges the 2D array into a 1D array, so we have to convert it into a 2D array afterwards
sortMat(nsr, len(nsr), len(nsr[0]))
nsr_sorted = printMat(nsr, len(nsr), len(nsr[0]))

# Let's make it a 2-D array. Now we have our imagette sorted in increasing order of NSR
nsr_sorted = np.reshape(np.array(nsr_sorted), (-1, 6))

# Let's plot the imagette of the system sorted in increasing order of NSR
ploting_nsr_s(nsr_sorted)
#print('This is nsr_sorted:', nsr_sorted)
#indexes(nsr_sorted)
#plt.plot(nsr_sorted)
#plt.show()
print("This is nsr_sorted:\n", tabulate(nsr_sorted, showindex = True))
print("This is f_pix_t:\n", tabulate(f_pix_t, showindex = True))
print("This is f_pix_c:\n", tabulate(f_pix_c, showindex = True))

# Let's obtain NSRagg
#print(f_pix_t[2][1])
#f_pix_t_sorted = [[60133.12290378712, 24684.806589619937, 24542.877980158522, 20337.17392453979,
# 5176.56164111941, 5025.649122919861],
# [2262.122495221013, 1911.710429704617, 607.7473293170398, 566.7135096363473,
# 456.5729812377677, 363.92038952618975],
# [491.35546208904026, 2262.122495221013, 236.5377805345862, 236.5377805345862,
# 123.33333805675579, 114.49540630558229],
# [99.61635088462924, 78.09638270509944, 35.1074921204392, 25.967710847174022,
# 24.93052680007022, ],
# [],[]]
f_pix_t_sorted = [[f_pix_t[2][3], f_pix_t[2][2], f_pix_t[3][2], f_pix_t[3][3], f_pix_t[4][2], f_pix_t[4][3]],
                [f_pix_t[2][1], f_pix_t[3][1], f_pix_t[1][2], f_pix_t[4][1], f_pix_t[1][3], f_pix_t[2][0]],
                [f_pix_t[3][4], f_pix_t[3][0], f_pix_t[5][2], f_pix_t[5][3], f_pix_t[2][4], f_pix_t[1][1]],
                [f_pix_t[5][1], f_pix_t[4][0], f_pix_t[0][2], f_pix_t[1][0], f_pix_t[0][1], f_pix_t[5][0]],
                [f_pix_t[0][3], f_pix_t[1][4], f_pix_t[4][4], f_pix_t[5][4], f_pix_t[2][5], f_pix_t[3][5]],
                [f_pix_t[0][0], f_pix_t[0][4], f_pix_t[1][5], f_pix_t[4][5], f_pix_t[0][5], f_pix_t[5][5]]]

np.savetxt("f_pix_t_sorted.csv", f_pix_t_sorted)
#plt.plot(f_pix_t_sorted)
#plt.show()

f_pix_c_sorted = [[f_pix_c[2][3], f_pix_c[2][2], f_pix_c[3][2], f_pix_c[3][3], f_pix_c[4][2], f_pix_c[4][3]],
                [f_pix_c[2][1], f_pix_c[3][1], f_pix_c[1][2], f_pix_c[4][1], f_pix_c[1][3], f_pix_c[2][0]],
                [f_pix_c[3][4], f_pix_c[3][0], f_pix_c[5][2], f_pix_c[5][3], f_pix_c[2][4], f_pix_c[1][1]],
                [f_pix_c[5][1], f_pix_c[4][0], f_pix_c[0][2], f_pix_c[1][0], f_pix_c[0][1], f_pix_c[5][0]],
                [f_pix_c[0][3], f_pix_c[1][4], f_pix_c[4][4], f_pix_c[5][4], f_pix_c[2][5], f_pix_c[3][5]],
                [f_pix_c[0][0], f_pix_c[0][4], f_pix_c[1][5], f_pix_c[4][5], f_pix_c[0][5], f_pix_c[5][5]]]

np.savetxt("f_pix_c_sorted.csv", f_pix_c_sorted)
#plt.plot(f_pix_c_sorted)
#plt.show()

ft = open('f_pix_t_sorted.csv')
fc = open('f_pix_c_sorted.csv')

f_pix_t_s = np.loadtxt(ft, delimiter=' ')
f_pix_c_s = np.loadtxt(fc, delimiter=' ')
#print(f_pix_t_s)

# making them 1-D arrays
f_pix_t_s = np.ravel(f_pix_t_s)
f_pix_c_s = np.ravel(f_pix_c_s)

#print('This is f_pix_t_s:\n', f_pix_t_s)
#print('This is f_pix_c_s:\n', f_pix_c_s)
n_nsragg = np.array(np.sqrt(numerator_agg(45., 50.2, 7.2, f_pix_t_s, f_pix_c_s)))
d_nsragg = np.array(denominator_agg(f_pix_t_s))
#d_nsragg_contaminant = np.array(denominator_agg_contaminant(f_pix_c_s))

#print(n_nsragg)
#print(d_nsragg)
#print(d_nsragg_contaminant)

#plt.plot(f_pix_t_s, label = 'ft')
#plt.plot(f_pix_c_s, label = 'fc')
#plt.legend()
#plt.show()

NSR_agg = nsr_agg(n_nsragg,d_nsragg)

NSR1h = nsr1h((10**6)/(12*np.sqrt(24)),NSR_agg)

plt.plot(NSR1h, 'o-')
plt.xlabel('Number of pixels $m$  composing the binary mask', fontsize=14)
plt.ylabel('$ NSR_{1h}[ppm \, \sqrt{hr}] $', fontsize = 14)
plt.title('NSR evolution curve for the target mask', fontsize = 14)
#plt.savefig('binary_mask.pdf', format='pdf', bbox_inches='tight', dpi=300)
plt.show()

print(np.where( NSR1h == np.min(NSR1h)))

print(f'\n########## Now we are centered in the contaminant ##########\n')

#f_pix_c[f_pix_c == 0] = 0.08
# Let's do the same but for the contaminant now. First, we obtain the NSR of the contaminant
nsr_c = NSRn(45., 50.2, 7.2, f_pix_c, f_pix_t)

# Let's plot the imagette of the system in terms of NSR for the contaminant
ploting_nsr(nsr_c)
print("This is nsr_c:\n", tabulate(nsr_c, showindex = True))
#print(nsr_c[5][4])
table = nsr_c

# Let's sort the NSRn in increasing order. This is, rearrange the pixels in increasing order of NSRn.
# This algorithm rearranges the 2D array into a 1D array, so we have to convert it into a 2D array afterwards
sortMat(nsr_c, len(nsr_c), len(nsr_c[0]))
nsr_c_sorted = printMat(nsr_c, len(nsr_c), len(nsr_c[0]))

# Let's make it a 2-D array. Now we have our imagette sorted in increasing order of NSR
nsr_c_sorted = np.reshape(np.array(nsr_c_sorted), (-1, 6))

# Let's plot the imagette of the system sorted in increasing order of NSR
ploting_nsr_s(nsr_c_sorted)

print("This is nsr_c_sorted:\n", tabulate(nsr_c_sorted, showindex = True))
print("This is f_pix_c:\n", tabulate(f_pix_c, showindex = True))

#Obtaining the flux values per pixel from the sorted NSR for the contaminant point of view
f_pix_t_sorted_contaminant = [[f_pix_t[3][5], f_pix_t[4][4], f_pix_t[3][4], f_pix_t[4][5], f_pix_t[5][4], f_pix_t[5][5]],\
                             [f_pix_t[4][3], f_pix_t[3][3], f_pix_t[2][4], f_pix_t[5][3], f_pix_t[2][5], f_pix_t[4][2]],\
                             [f_pix_t[3][2], f_pix_t[5][2], f_pix_t[1][4], f_pix_t[3][1], f_pix_t[2][3], f_pix_t[4][1]],\
                             [f_pix_t[1][3], f_pix_t[1][5], f_pix_t[2][2], f_pix_t[5][1], f_pix_t[1][2], f_pix_t[0][4]],\
                             [f_pix_t[0][3], f_pix_t[2][1], f_pix_t[0][5], f_pix_t[0][2], f_pix_t[1][1], f_pix_t[0][1]],\
                             [f_pix_t[0][0], f_pix_t[5][0], f_pix_t[1][0], f_pix_t[4][0], f_pix_t[3][0], f_pix_t[2][0]]]

np.savetxt("f_pix_t_sorted_contaminant.csv", f_pix_t_sorted_contaminant)


f_pix_c_sorted_contaminant = [[f_pix_c[3][5], f_pix_c[4][4], f_pix_c[3][4], f_pix_c[4][5], f_pix_c[5][4], f_pix_c[5][5]],
                            [f_pix_c[4][3], f_pix_c[3][3], f_pix_c[2][4], f_pix_c[5][3], f_pix_c[2][5], f_pix_c[4][2]],
                            [f_pix_c[3][2], f_pix_c[5][2], f_pix_c[1][4], f_pix_c[3][1], f_pix_c[2][3], f_pix_c[4][1]],
                            [f_pix_c[1][3], f_pix_c[1][5], f_pix_c[2][2], f_pix_c[5][1], f_pix_c[1][2], f_pix_c[0][4]],
                            [f_pix_c[0][3], f_pix_c[2][1], f_pix_c[0][5], f_pix_c[0][2], f_pix_c[1][1], f_pix_c[0][1]],
                            [f_pix_c[0][0], f_pix_c[5][0], f_pix_c[1][0], f_pix_c[4][0], f_pix_c[3][0], f_pix_c[2][0]]]

np.savetxt("f_pix_c_sorted_contaminant.csv", f_pix_c_sorted_contaminant)

# Opening those csv
ft_contaminant = open('f_pix_t_sorted_contaminant.csv')
fc_contaminant = open('f_pix_c_sorted_contaminant.csv')


f_pix_t_s_contaminant = np.loadtxt(ft_contaminant, delimiter=' ')
f_pix_c_s_contaminant = np.loadtxt(fc_contaminant, delimiter=' ')
#print(f_pix_t_s)

# making them 1-D arrays
f_pix_t_s_contaminant = np.ravel(f_pix_t_s_contaminant)
f_pix_c_s_contaminant = np.ravel(f_pix_c_s_contaminant)


n_nsragg_contaminant = np.array(np.sqrt(numerator_agg(45., 50.2, 7.2, f_pix_c_s_contaminant, f_pix_t_s_contaminant)))
d_nsragg_contaminant = np.array(denominator_agg(f_pix_c_s_contaminant))



NSR_agg_contaminant = nsr_agg(n_nsragg_contaminant, d_nsragg_contaminant)

NSR1h_contaminant = nsr1h((10**6)/(12*np.sqrt(24)),NSR_agg_contaminant)

plt.plot(NSR1h_contaminant, 'o-')
plt.xlabel('Number of pixels $m$  composing the binary mask', fontsize=14)
plt.ylabel('$ NSR_{1h}[ppm \, \sqrt{hr}] $', fontsize = 14)
plt.title('NSR evolution curve for the contaminant mask', fontsize = 14)
#plt.savefig('binary_mask.pdf', format='pdf', bbox_inches='tight', dpi=300)
plt.show()

print(np.where( NSR1h_contaminant == np.min(NSR1h_contaminant)))
# Let's calculate now the SPRt and SPRc

# First, let's make f_pix_t and f_pix_c 1-D arrays
f_pix_t_1d = np.ravel(f_pix_t)
f_pix_c_1d = np.ravel(f_pix_c)

# For the target, the weight is:

index_target = [60133.1]

print(f'This is f_pix_t_1d:\n', f_pix_t_1d)
print(f_pix_t_1d[15])

wt = np.zeros(36)
wt[13] = 1
wt[14] = 1
wt[15] = 1
wt[19] = 1
wt[20] = 1
wt[21] = 1
wt[26] = 1
wt[27] = 1

print(wt)
# For the contaminant, the weight is:

print(f'This is f_pix_c_1d:\n', f_pix_c_1d)
#print(f_pix_t_1d[15])

wc = np.zeros(36)
wc[22] = 1
wc[23] = 1
wc[28] = 1
wc[29] = 1

print(wc)

# Now, we can compute the SPRt and SPRc

# SPRT #

# Let's compute the numerator of the SPRt
num_sprt = np.array(numerator_SPR_t(f_pix_c_1d, wt))

# Let's compute the denominator of the SPRt
den_sprt = np.array(denominator_SPR_t(f_pix_t_1d, f_pix_c_1d, wt))

# Let's compute the SPRt
spr_t = sprt(num_sprt,den_sprt)

SPRT = spr_t[-1]

print(f'SPRT is:\n', SPRT)

# SPRC #

# Let's calculate the numerator of the SPRc
num_sprc = np.array(numerator_SPR_c(f_pix_t_1d, wc))

# Let's calculate the denominator of the SPRc
den_sprc = np.array(denominator_SPR_c(f_pix_t_1d, f_pix_c_1d, wc))

# Let's compute the SPRc
spr_c = sprc(num_sprc,den_sprc)

SPRC = spr_c[-1]

print(f'SPRC is:\n', SPRC)

# Let's calculate, finally, the etas

eta_t = etat(SPRT = SPRT, td = 13, ntr_t = 3, dbeb = 84, nsr = NSR1h)

eta_c = etac(SPRC = SPRC, td = 13, ntr_c = 3, dbeb = 84, nsr = NSR1h_contaminant)

print(r"\eta_{t} is:", "%.2f" % eta_t)
print(r"\eta_{c} is:", "%.2f" % eta_c)
#print((1 -0.007212222892889512 ) * np.sqrt(13 * 3)*84/min(NSR1h))
# Let's calculate the delta_obs
delta_obs_c = (SPRC)*84

delta_obs_t = (1 - SPRT)*84

print(r"This is delta_obs_target:", "%.2f" % delta_obs_t)
print(r"This is delta_obs_contaminant:",  "%.2f" % delta_obs_c)
