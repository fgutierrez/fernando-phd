import matplotlib.pyplot as plt

# This function plots the imagettes for both target and contaminant
def ploting_imagettes(rows, cols, ft, fc):
    fig, axs = plt.subplots(rows, cols)
    axs[0].imshow(ft, origin='lower')
    axs[0].set_title(f'Target Imagette')
    axs[0].set_xticks([0, 1, 2, 3, 4, 5])
    axs[0].set_xticklabels([0, 1, 2, 3, 4, 5])
    axs[1].imshow(fc, origin='lower')
    axs[1].set_title(f'Contaminant Imagette')
    axs[1].set_xticks([0, 1, 2, 3, 4, 5])
    axs[1].set_xticklabels([0, 1, 2, 3, 4, 5])
    fig.tight_layout()
    plt.show()

# This function plots the unsorted nsr imagette
def ploting_nsr(n):
    plt.imshow(n, origin='lower')
    plt.title(f'NSR imagette')
    plt.show()

# This function plots the sorted nsr imagette
def ploting_nsr_s(n):
    plt.imshow(n, origin='lower')
    plt.title(f'NSR imagette sorted in increasing order')
    plt.show()


# plt.figure()
# plt.imshow(f_pix_t, origin='lower')
# plt.show()
# plt.imshow(f_pix_c, origin='lower')
# plt.show()
