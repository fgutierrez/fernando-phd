class planets:
    def __init__(self, name, depth, duration, number):
        self.name = name  # planet's name
        self.depth = depth  # planet's transit depth in ppm
        self.duration = duration  # planet's transit duration in hours
        self.number = number  # planet's number of transits


def print_etas(name, value):
    print("The statistical significance of a", name, '-like planet is', "%.2f" % value)
