# Let's calculate the numerator of the SPRt (target)
def numerator_SPR_t(F,w):
    j = [F[0]*w[0]]
    k = F[0]*w[0]
    for i in range(1,len(F)):
        k = k + F[i]*w[i]
        j.append(k)
    return j

# Let's calculate the denominator of the SPRt (target)
def denominator_SPR_t(F,Fc,w):
    l = [(F[0] + Fc[0])*w[0]]
    m = (F[0] + Fc[0])*w[0]
    for i in range(1,len(F)):
        m = m + (F[i] + Fc[i])*w[i]
        l.append(m)
    return l

# Let's calculate the SPRt
def sprt(a,b):
    return a/b

# Let's calculate the numerator of the SPRc (contaminant)
def numerator_SPR_c(F,w):
    n = [F[0]*w[0]]
    o = F[0]*w[0]
    for i in range(1,len(F)):
        o = o + F[i]*w[i]
        n.append(o)
    return n

# Let's calculate the denominator of the SPRc (contaminant)
def denominator_SPR_c(F,Fc,w):
    l = [(F[0] + Fc[0])*w[0]]
    m = (F[0] + Fc[0])*w[0]
    for i in range(1,len(F)):
        m = m + (F[i] + Fc[i])*w[i]
        l.append(m)
    return l

# Let's calculate the SPRt
def sprc(x,y):
    return x/y
