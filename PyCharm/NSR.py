import numpy as np

# Let's calculate the NSR of the system
def NSRn(sb, sd, sq, ft, fc):
    n = np.sqrt(ft + fc + sb ** 2 + sd ** 2 + sq ** 2) / ft
    return n

# Let's calculate the denominator of the aggregate noise-to-signal NSRagg(m)  for both the target and the contaminant
def denominator_agg(F):
    a = [F[0]]
    b = F[0]
    for i in range(1,len(F)):
        b = b + F[i]
        a.append(b)
    return a

# Let's calculate the numerator of NSR_agg(m) of target
def numerator_agg(sb,sq,sd,f,fc):
    c = []
    d = 0
    for i in range(0,len(f)):
        d = d + sb**2 + sq*2 + sd**2 + f[i] + fc[i]
        c.append(d)
    return c


# Let's create the fraction for the nsr
def nsr_agg(n,d):
    return n/d


def nsr1h(z,nsr):
    return z*nsr

if __name__ == '__main__':
    sb = 45.
    sd = 50.2
    sq = 7.2
    ft = np.array([[7.58315155e+00, 2.49305268e+01, 3.51074921e+01, 1.52195405e+01,
        7.12060013e+00, 3.29100925e+00],
       [2.59677108e+01, 1.14495406e+02, 6.07747329e+02, 4.56572981e+02,
        1.42753133e+01, 5.64649181e+00],
       [3.63920390e+02, 2.26212250e+03, 2.46848066e+04, 6.01331229e+04,
        1.23333338e+02, 9.13232219e+00],
       [2.60116624e+02, 1.91171043e+03, 2.45428780e+04, 2.03371739e+04,
        4.91355462e+02, 1.49755736e+01],
       [7.80963827e+01, 5.66713510e+02, 5.17656164e+03, 5.02564912e+03,
        1.85915804e+01, 6.36678884e+00],
       [2.06379688e+01, 9.96163509e+01, 2.36537781e+02, 1.23916140e+02,
        1.02352055e+01, 2.99118495e+00]])
    fc = np.array([[1.60856289e-05, 3.23744445e-05, 3.65398073e-05, 2.79419006e-05,
        1.91250552e-05, 1.03936218e-05],
        [5.12425154e-05, 1.68465960e-04, 2.37235956e-04, 1.02844778e-04,
        4.81168627e-05, 2.22387211e-05],
        [1.75474645e-04, 7.73693179e-04, 4.10680200e-03, 3.08525392e-03,
        9.64642417e-05, 3.81556984e-05],
        [2.45916174e-03, 1.52861045e-02, 1.66805526e-01, 4.06344573e-01,
        8.33414767e-04, 6.17109071e-05],
        [1.75771644e-03, 1.29182241e-02, 1.65846455e-01, 1.37426760e-01,
        3.32029364e-03, 1.01196192e-04],
        [5.27729806e-04, 3.82951937e-03, 3.49801844e-02, 3.39604057e-02,
        1.25631057e-04, 4.30230457e-05]])

    print(NSRn(sb, sd, sq, ft, fc))